var http = require('http');
var mongodb = require('mongodb');
var cookies = require('cookies');
var uuid = require('uuid');
var qs = require('query-string');

var lib = require('./lib');
var common = require('./common');
var rest = require('./rest');

var config = {
    port: 8888,
    dbUrl: "mongodb://localhost:27017",
    database: "pwa2019"
};

var httpServer = http.createServer();

httpServer.on('request', function (req, rep) {

    var appCookies = new cookies(req, rep);
    var session = appCookies.get('session');
    var now = Date.now();
    if(!session || !common.sessions[session]) {
        session = uuid();
        console.log('Created new session ' + session);
        common.sessions[session] = { from: req.connection.remoteAddress, created: now, touched: now };
    } else {
        common.sessions[session].touched = now;
    }
    appCookies.set('session', session, { httpOnly: false });
    
    var parsedUrl = qs.parseUrl(req.url);
    
    console.log(req.method + ' ' + req.url + ' [' + session + ']');
    
	if(parsedUrl.url == '/') {
		lib.serveStaticContent(rep, 'html/index.html');
	} else if(parsedUrl.url == '/favicon.ico') {
		lib.serveStaticContent(rep, 'img/favicon.ico');
    } else if(/^\/(html|css|js|fonts|img)\//.test(parsedUrl.url)) {
        lib.serveStaticContent(rep, '.' + parsedUrl.url);
    } else {
        var endpoint = parsedUrl.url.substr(1);
        if(rest[endpoint] && typeof rest[endpoint] === "function") {
            if(req.method == 'POST' || req.method == 'PUT') {
                lib.getPayload(req, rep, function(req, rep, err, payload) {
                    if(err) 
                        lib.sendJSON(rep, 400, 'Payload parsing error', {err: 'Payload parsing error'});
                    else {
                        console.log("rest." + endpoint + " query=" + JSON.stringify(parsedUrl.query) + " payload=" + JSON.stringify(payload));
                        rest[endpoint](req, rep, session, parsedUrl.query, payload);
                    }
                });
            } else {
                console.log("rest." + endpoint + " query=" + JSON.stringify(parsedUrl.query) + " payload=null");
                rest[endpoint](req, rep, session, parsedUrl.query, null);
            }
        } else {
            lib.sendJSON(rep, 400, 'Unhandled endpoint ' + parsedUrl.url, {err: 'Unhandled endpoint ' + parsedUrl.url});
        }
    }
});

mongodb.MongoClient.connect(config.dbUrl, { useUnifiedTopology: true }, function(err, conn) {
    if(err) {
        console.error("Connection to database failed");
        process.exit(0);
    }
    console.log("Connection to database established");
    var db = conn.db(config.database);
    common.accounts = db.collection("accounts");
    common.history = db.collection("history");
    common.predefined = db.collection("predefined");
    try {
        httpServer.listen(config.port);
    } catch(ex) {
        console.log("Port " + config.port + " cannot be used");
        process.exit(0);
    }
    console.log("HTTP server is listening on the port " + config.port);   
});