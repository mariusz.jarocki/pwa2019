var app1 = angular.module("app1", ['ngRoute', 'ngSanitize', 'ngAnimate', 'ui.bootstrap']);

app1.constant('routes', [
	{ route: '/', templateUrl: '/html/home.html', controller: 'Home', controllerAs: 'ctrl', menu: '<i class="fa fa-lg fa-home"></i>' },
	{ route: '/transfer', templateUrl: '/html/transfer.html', controller: 'Transfer', controllerAs: 'ctrl', menu: 'Transfer' },
	{ route: '/history', templateUrl: '/html/history.html', controller: 'History', controllerAs: 'ctrl', menu: 'History' },
	{ route: '/settings', templateUrl: '/html/settings.html', controller: 'Settings', controllerAs: 'ctrl', menu: 'Settings' }
]);

app1.config(['$routeProvider', '$locationProvider', 'routes', function($routeProvider, $locationProvider, routes) {
    $locationProvider.hashPrefix('');
	for(var i in routes) {
		$routeProvider.when(routes[i].route, routes[i]);
	}
	$routeProvider.otherwise({ redirectTo: '/' });
}]);

app1.service('common', ['$http', '$uibModal', 'routes', function($http, $uibModal, routes) {
	var common = this;
	common.session = {
		login: null,
		menu: []
	};

	common.rebuildMenu = function() {
		common.session.menu = [];
		for(var i in routes) {
			if(routes[i].menu && (i==0 || common.session.login)) { // only home menu(i==0) for not-logged-in, rest is hidden
				common.session.menu.push({route: routes[i].route, title: routes[i].menu});
			}
		}
	};

	/* ask backend who is logged in */
	$http.get('/login').then(
		function(rep) {
			common.session.login = rep.data.login;
			common.rebuildMenu();
		}
	);

	common.confirm = function(text, callback) {
		console.log(text);
		var modalInstance = $uibModal.open({
			animation: true,
			ariaLabelledBy: 'modal-title-top',
			ariaDescribedBy: 'modal-body-top',
			templateUrl: '/html/confirmation.html',
			controller: 'Confirmation',
			controllerAs: 'ctrl',
		});
		modalInstance.result.then(
			function(data) {
				callback(data);
			});
	};

}]);

app1.controller('Menu', [ '$scope', '$location', 'common', function($scope, $location, common) {
	var ctrl = this;
	ctrl.session = common.session;

	ctrl.navClass = function(page) {
		return page === $location.path() ? 'active' : '';
	}

	ctrl.isCollapsed = true;
	$scope.$on('$routeChangeSuccess', function () {
		ctrl.isCollapsed = true;
	});

}]);

app1.controller("Confirmation", [ "$uibModalInstance", function($uibModalInstance) {
    console.log("Confirmation controller started");
    var ctrl = this;
    ctrl.submit = function(answer) {
        $uibModalInstance.close(answer);
    }
}]);